/*  Si7021-sensor
 *  Copyright (C) 2016 Rich Mattes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef SI7021_H
#define SI7021_H

class Si7021
{
  public:
    /*! @brief Create Si7021 interface
     * Create a new Si7021 interface object
     * @param i2c_dev The i2c bus that the sensor is on (e.g. /dev/i2c-N)
     */
    Si7021(int i2c_dev);
    ~Si7021();

    /*! @brief Read temperature from device
     * Initiate a temperature reading, and return the result.
     * @return Temperature, in degrees C
     */
    float temperature();

    /*! @brief Read relative humidity from device
     * Initiate a relative humidity (RH) reading, and return the result.
     * @return RH, in the range [0 1]
     */
    float humidity();

  private:
    int i2c_fd_;
};

#endif
