# Si7021 Sensor
This project contains a library to talk to a Si7021 temperature sensor via
the I2C protocol with Linux.

https://www.sparkfun.com/products/13763

It also contains a test application to read and print current values, and a
ROS node to publish the values via ROS topics

## ROS Node
The ROS node publishes data over two topics by default:

- /temperature: The temperature, in degrees C
- /relative_humidity: The relative humidity in percent / 100 (e.g. [0,1])

Data is time-stamped and published once per second.

## License
The library and ROS node are provided under the GPLv2+ license.  See the
COPYING file for more details.
