/*  Si7021-sensor
 *  Copyright (C) 2016 Rich Mattes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "Si7021.h"

#include <errno.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

const char MEASURE_HUMIDITY_HOLD = 0xE5;
const char MEASURE_HUMIDITY_NO_HOLD = 0xF5;
const char MEASURE_TEMP_HOLD = 0xE3;
const char MEASURE_TEMP_NO_HOLD = 0xF3;

Si7021::Si7021(int i2c_dev) : i2c_fd_(0)
{
    std::stringstream filename;
    filename << "/dev/i2c-" << i2c_dev;

    i2c_fd_ = open(filename.str().c_str(), O_RDWR);

    if (i2c_fd_ < 0) {
        throw std::runtime_error(std::string("Failed to open ") +
                                 filename.str() + std::string(": ") +
                                 std::string(strerror(errno)));
    }

    int addr = 0x40;
    if (ioctl(i2c_fd_, I2C_SLAVE, addr) < 0) {
        throw std::runtime_error(std::string("Failed to call ioctl: ") +
                                 std::string(strerror(errno)));
    }
}

Si7021::~Si7021()
{
    if (i2c_fd_ > 0) {
        close(i2c_fd_);
    }
}

float Si7021::temperature()
{
    char response[2];
    if (write(i2c_fd_, &MEASURE_TEMP_HOLD, 1) < 1) {
        return 0;
    }

    int nread = read(i2c_fd_, response, 2);
    if (nread < 0) {
        return 0;
    }

    unsigned short temp = response[0] << 8 | response[1];
    float real_temp = (float)temp * 175.72F / 65536.0F - 46.85;
    return real_temp;
}

float Si7021::humidity()
{
    char response[2];
    if (write(i2c_fd_, &MEASURE_HUMIDITY_HOLD, 1) < 1) {
        return 0;
    }

    int nread = read(i2c_fd_, response, 2);
    if (nread < 0) {
        return 0;
    }

    unsigned short humid = response[0] << 8 | response[1];
    float real_humid = ((((float)humid * 125.0F) / 65535.0F) - 6.0F) / 100.0F;
    return real_humid;
}
