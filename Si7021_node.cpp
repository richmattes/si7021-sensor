/*  Si7021-sensor
 *  Copyright (C) 2016 Rich Mattes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "Si7021.h"
#include "ros/ros.h"
#include "sensor_msgs/RelativeHumidity.h"
#include "sensor_msgs/Temperature.h"
int main(int argc, char *argv[])
{
    ros::init(argc, argv, "Si7021");

    ros::NodeHandle n;
    ros::Publisher temp_pub =
        n.advertise<sensor_msgs::Temperature>("temperature", 10);
    ros::Publisher humid_pub =
        n.advertise<sensor_msgs::RelativeHumidity>("relative_humidity", 10);

    ros::Rate loop_rate(1);

    Si7021 dev(1);

    while (ros::ok()) {
        ros::Time time = ros::Time::now();

        sensor_msgs::Temperature temp;
        temp.temperature = dev.temperature();
        temp.header.stamp = time;

        sensor_msgs::RelativeHumidity humid;
        humid.relative_humidity = dev.humidity();
        humid.header.stamp = time;

        temp_pub.publish(temp);
        humid_pub.publish(humid);

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
