/*  Si7021-sensor
 *  Copyright (C) 2016 Rich Mattes
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "Si7021.h"

#include <iostream>

int main(int /*argc*/, char** /*argv*/)
{
    Si7021 dev(1);

    std::cout << "Temperature: " << dev.temperature() * 1.8F + 32.0F << " F\n";
    std::cout << "Relative Humidity: " << dev.humidity() * 100.0F << "%\n";
}
